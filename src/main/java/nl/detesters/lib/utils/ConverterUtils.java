package nl.detesters.lib.utils;

import nl.detesters.lib.converters.BigDecimalConverter;
import nl.detesters.lib.converters.DateTimeConverter;
import org.jbehave.core.steps.ParameterConverters;

import java.util.ArrayList;
import java.util.List;

/**
 *
 */
public final class ConverterUtils {
    public static ParameterConverters.ParameterConverter[] customConverters() {
        List<ParameterConverters.ParameterConverter> converters = new ArrayList<>();
        converters.add(new BigDecimalConverter());
        converters.add(new DateTimeConverter());

        return converters.toArray(new ParameterConverters.ParameterConverter[converters.size()]);
    }
}
