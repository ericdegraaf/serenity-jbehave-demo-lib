package nl.detesters.lib.converters;

import org.jbehave.core.steps.ParameterConverters;

import java.lang.reflect.Type;
import java.math.BigDecimal;


/**
 *
 */
public class BigDecimalConverter implements ParameterConverters.ParameterConverter {
    @Override
    public boolean accept(Type type) {
        if (type instanceof Class<?>) {
            return BigDecimal.class.isAssignableFrom((Class<?>) type);
        }
        return false;
    }

    @Override
    public Object convertValue(String value, Type type) {
        return new BigDecimal(value);
    }
}
